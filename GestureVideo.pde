/*
Simple video scrubbing example using Leap Motion
Made for Processing – requires LeapMotionP5 library

An open hand pauses the video and then allows you to scrub the video left and right.
Closing your hand plays the video.

Note you may need to get the correct size of the video from Quicktime info.
*/

import processing.video.*;
import javax.swing.*; 
import java.io.*;

import com.onformative.leap.LeapMotionP5;
import com.leapmotion.leap.Hand;
import com.leapmotion.leap.Finger;



Movie movie;

LeapMotionP5 leap;

boolean isPaused = false;

void setup(){
	size(displayWidth,displayHeight);
	leap = new LeapMotionP5(this);
	selectFile();
}

void draw(){
	background(0);

	image(movie, width/2-movie.width/2, height/2-movie.height/2, movie.width, movie.height);
  ArrayList fingers = leap.getFingerList();
	
	if (fingers.size() > 1) {
		isPaused = true;
		movie.pause();
	} else {
		isPaused = false;
		movie.play();
	}
  if (isPaused) {
    
    Finger f1 = (Finger) fingers.get(0);
    PVector p1 = leap.getTip(f1);
    showTime(p1.x);
    for (Hand hand: leap.getHandList()) {
      PVector handPos = leap.getPosition(hand);
      if (handPos != null ) {
        int framePos = int(map(handPos.x,0,displayWidth,0,movie.duration()));
        movie.jump(framePos);
        movie.play();
        movie.pause();  
      }
    }
  }
	
		
}

void showTime(float x) {

    stroke(255);
    line(x, 0, x, height);
    noStroke();
    fill(0);
    rect(x+1, height/2-20, 100, 30);
    fill(255);
    textSize(18);
    text(movie.time()/60, x, height/2); 

}



void movieEvent(Movie m) {
  m.read();

  
}

void selectFile() {
 try { 
  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); 
} catch (Exception e) { 
  e.printStackTrace();  
}  
final JFileChooser fc = new JFileChooser(); 
int chosenFile = fc.showOpenDialog(this); 
if (chosenFile == JFileChooser.APPROVE_OPTION) { 
    File file = fc.getSelectedFile(); 
    initializeMovieWithFile(file.getPath());     
  } else {
    exit();
  }
  
}

void initializeMovieWithFile(String filePath){
  movie = null;
  movie = new Movie(this, filePath);
  movie.play();
}

public void stop() {
  leap.stop();
}