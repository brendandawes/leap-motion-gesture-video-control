# Basic video control using gestures in Leap Motion

This is a simple example – coded in Processing using the LeapMotionP5 library – to control video. 

- Open hand to pause video
- Move hand left or right to scrub through the frames of the video
- Close hand to continue playing video from where you scrubbed too

You can see an example of it in action at [http://brendandawes.com/blog/leapmotion](http://brendandawes.com/blog/leapmotion)